/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.microcrm.methods;

import fi.datadevils.microcrm.model.User;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Samuli
 */
public class Authenticate {

    public static User authenticateUser(User u, String password) {
        try {
            byte hash[] = getPasswordHash(password, u.getSalt());

            System.out.println(hash);
            System.out.println(u.getHash());
            
            if (Arrays.equals(hash, u.getHash())) {
                return u;
            }
        } catch (Exception e) {
            
        }
        return null;
    }

    public static byte[] getPasswordHash(String password, String salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        digest.reset();
        digest.update(salt.getBytes("UTF-8"));
        System.out.println("getPasswordHash: " + password + " " + salt);
        byte[] hash = digest.digest(password.getBytes("UTF-8"));
        System.out.println("Hash: " + hash.toString());
        return hash;
    }

    public static String generateSalt() {
        byte[] salt = new byte[4];
        final Random r = new SecureRandom();

        r.nextBytes(salt);
        
        return salt.toString();
    }

    public static String[] decodeAuthorization(String auth) {

        //Replacing "Basic THE_BASE_64" to "THE_BASE_64" directly
        auth = auth.replaceFirst("[B|b]asic ", "");

        //Decode the Base64 into byte[]
        byte[] decodedBytes = DatatypeConverter.parseBase64Binary(auth);

        //If the decode fails in any case
        if (decodedBytes == null || decodedBytes.length == 0) {
            return null;
        }

        //Now we can convert the byte[] into a splitted array :
        //  - the first one is login,
        //  - the second one password
        return new String(decodedBytes).split(":", 2);
    }
}
