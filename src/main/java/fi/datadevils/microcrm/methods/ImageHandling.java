/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.microcrm.methods;

import fi.datadevils.microcrm.model.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;

/**
 *
 * @author Samuli
 */
public class ImageHandling {

    // save uploaded file to new location
    public static void writeToFile(InputStream uploadedInputStream,
            String uploadedFileLocation) {

        try {
            OutputStream out = new FileOutputStream(new File(
                    uploadedFileLocation));
            int read = 0;
            byte[] bytes = new byte[1024];

            out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    public static Image uploadProfilePicture(String fileName, boolean person) {

        if (fileName == null) {
            if (person) {
                fileName = "PersonPlaceholder.png";
            } else {
                fileName = "LogoPlaceholder.png";
            }
        }

        try {
            Image image = new Image();
            File file = new File(fileName);

            image.setImage(Files.readAllBytes(file.toPath()));
            image.setFileName(fileName);

            return image;
        } catch (Exception e) {

        }
        return null;
    }
}
