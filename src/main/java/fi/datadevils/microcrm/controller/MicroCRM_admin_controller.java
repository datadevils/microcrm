/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.microcrm.controller;

import fi.datadevils.microcrm.Constants;
import fi.datadevils.microcrm.model.Basket;
import fi.datadevils.microcrm.model.Company;
import fi.datadevils.microcrm.model.Contact;
import fi.datadevils.microcrm.model.MicroCRM_EJB;
import fi.datadevils.microcrm.model.Person;
import fi.datadevils.microcrm.model.Task;
import fi.datadevils.microcrm.model.User;
import fi.datadevils.microcrm.rest.classes.NewBasket;
import fi.datadevils.microcrm.rest.classes.NewContact;
import fi.datadevils.microcrm.rest.classes.NewFollowupTask;
import fi.datadevils.microcrm.rest.classes.NewTask;
import fi.datadevils.microcrm.rest.classes.NewUser;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

/**
 *
 * @author mika
 */
@Named(value = "microCRM_admin_controller")
@SessionScoped
public class MicroCRM_admin_controller implements Serializable {

    @EJB
    MicroCRM_EJB ejb;

    private String searchText;
    private String message;
    private List<Company> companyList = new ArrayList<>();
    private List<Person> personList = new ArrayList<>();
    private List<Contact> contactList = new ArrayList<>();
    private List<Task> taskList = new ArrayList<>();
    private String middleInclude;

    /**
     * Creates a new instance of microCRM_admin_controller
     */
    public MicroCRM_admin_controller() {
    }

    @PostConstruct
    public void init() {

        setMessage("Welcome to admin client!");
        ejb.synchroniceToDataBase();
        setCompanyList(ejb.listCompanies());
        setPersonList(ejb.listPersons());
        setTaskList(ejb.listTasks());
        //setContactList(ejb.listContacts());
    }

    public void clickUserManagement() {
        message = "Clicked User Management button!";
        //ejb.addPerson("Uuno Turhapuro", "Johtaja", "uuno@nokia.com", "555-1234", ejb.addCompany("Nokia"));
    }

    public void clickBasketManagement() {
        message = "Clicked Basket Management button!";
    }

    public void clickSearch() {
        message = "Clicked Search button!";
    }

    public void clickRow(String name) {
        message = "Clicked a row from a table! " + name;
    }

    public void renderCompanyManagement() {
        setMiddleInclude("adminFunctions/companyManagement.xhtml");
        setMessage("Clicked Company Management!");
    }

    public void renderUserManagement() {
        setMiddleInclude("adminFunctions/userManagement.xhtml");
        setMessage("Clicked User Management!");
    }

    public void renderBasketManagement() {
        setMiddleInclude("adminFunctions/basketManagement.xhtml");
        setMessage("Clicked Basket Management!");
    }

    public void renderPersonManagement() {
        setMiddleInclude("adminFunctions/personManagement.xhtml");
        setMessage("Clicked Person Management!");
    }

    public void renderTaskManagement() {
        setMiddleInclude("adminFunctions/taskManagement.xhtml");
        setMessage("Clicked Task Management!");
    }

    public void populateDatabase() {

        Company company = new Company();
        Person person = new Person();
        /*
         Task task1 = new Task();
         Task task2 = new Task();
        
         NewContact contact1 = new NewContact();
         NewFollowupTask followuptask2 = new NewFollowupTask();
         NewContact contact2 = new NewContact();
        
         company.setName("Testi Oy");
         company = ejb.addCompany(company.getName());
         person.setCompany(company);
         person.setEmail("pertti.perus@testi.fi");
         person.setName("Pertti Perus");
         person.setPhone("04012345678");
         person.setTitle("Testaaja");
         person = ejb.addPerson(person.getName(), person.getTitle(), person.getEmail(), person.getPhone(), company);
        
         Calendar cal = Calendar.getInstance();
         cal.set(2015, 3, 29);
         Date dueDate = cal.getTime();
         cal.set(2015, 3, 23);
         Date startDate = cal.getTime();
         task1.setDueDate(dueDate);
         task1.setStartDate(startDate);
         task1.setDescription("Testaa taskiketjua");
         task1.setPerson(person);
         task1.setDone(false);
         task1 = ejb.addTask(null, person, company,  startDate, dueDate, task1.getDescription(), task1.isDone(), null, null);
        
         contact1.setDate("2015-3-23");
         contact1.setDescription("Taskiketjun ekan taskin kontakti");
         contact1.setPerson(person);
         contact1.setTask(task1);
         contact1.setTaskdone(false);
         contact1.setType(1);
         task1 = ejb.addNewContact(contact1);
        
         followuptask2.setCompany(company);
         followuptask2.setPerson(person);
         followuptask2.setStart_date("2015-3-23");
         followuptask2.setDue_date("2015-3-29");
         followuptask2.setContact(task1.getFollowUp());
         followuptask2.setDescription("Jatka taskiketjun testaamista");
         task2 = ejb.addNewFollowupTask(followuptask2);
        
         contact2.setDate("2015-3-23");
         contact2.setDescription("Taskiketjun toisen taskin kontakti");
         contact2.setPerson(person);
         contact2.setTask(task2);
         contact2.setTaskdone(false);
         contact2.setType(1);
         task2 = ejb.addNewContact(contact2);
         */

        /////////////////////////////////////////////////////////////
        company = new Company();
        company.setName("Patria");
        company = ejb.addCompany(company.getName());
        person = new Person();
        person.setCompany(company);
        person.setEmail("pekka.tapiola@patria.fi");
        person.setName("Pekka Tapiola");
        person.setTitle("Osastopäällikkö");
        person.setPhone("0400 123321");
        person = ejb.addPerson(person.getName(), person.getTitle(), person.getEmail(),
                person.getPhone(), company);

        Company company2 = new Company();
        company2.setName("Accenture");
        company2 = ejb.addCompany(company2.getName());
        Person person2 = new Person();
        person2.setCompany(company2);
        person2.setEmail("heikki.peränne@accenture.fi");
        person2.setName("Heikki Peränne");
        person2.setTitle("Markkinointipäällikkö");
        person2.setPhone("0400 123456");
        person2 = ejb.addPerson(person2.getName(), person2.getTitle(), person2.getEmail(),
                person2.getPhone(), company2);

        Company company3 = new Company();
        company3.setName("Blancco");
        company3 = ejb.addCompany(company3.getName());
        Person person3 = new Person();
        person3.setCompany(company3);
        person3.setEmail("anja.mäkirinne@blancco.fi");
        person3.setName("Anja Mäkirinne");
        person3.setTitle("Henkilöstöpäällikkö");
        person3.setPhone("0400 654321");
        person3 = ejb.addPerson(person3.getName(), person3.getTitle(), person3.getEmail(),
                person3.getPhone(), company3);

        Task task1 = new Task();
        Calendar cal = Calendar.getInstance();
        cal.set(2015, 3, 15);
        Date dueDate = cal.getTime();
        cal.set(2015, 3, 10);
        Date startDate = cal.getTime();
        task1.setDueDate(dueDate);
        task1.setStartDate(startDate);
        task1.setDescription("Task1");
        task1.setCompany(company);
        task1.setPerson(person);
        task1.setDone(false);

        Task task2 = new Task();
        cal.set(2015, 3, 25);
        dueDate = cal.getTime();
        cal.set(2015, 3, 20);
        startDate = cal.getTime();
        task2.setDueDate(dueDate);
        task2.setStartDate(startDate);
        task2.setDescription("Task2");
        task2.setCompany(company);
        task2.setPerson(person);
        task2.setDone(false);

        Task task3 = new Task();
        cal.set(2015, 3, 16);
        dueDate = cal.getTime();
        cal.set(2015, 3, 15);
        startDate = cal.getTime();
        task3.setDueDate(dueDate);
        task3.setStartDate(startDate);
        task3.setDescription("Task3");
        task3.setCompany(company);
        task3.setPerson(person);
        task3.setDone(false);

        Task task4 = new Task();
        cal.set(2015, 4, 11);
        dueDate = cal.getTime();
        cal.set(2015, 4, 5);
        startDate = cal.getTime();
        task4.setDueDate(dueDate);
        task4.setStartDate(startDate);
        task4.setDescription("Task4");
        task4.setCompany(company);
        task4.setPerson(person2);
        task4.setDone(false);

        NewUser temp_user = new NewUser();
        User user1 = new User();
        temp_user.setName("Testi User");
        temp_user.setEmail("testi.user@hotmail.com");
        temp_user.setPassword("salasana");
        user1 = ejb.addNewUser(temp_user);

        temp_user = new NewUser();
        User user2 = new User();
        temp_user.setName("Testi User2");
        temp_user.setEmail("testi.user2@hotmail.com");
        temp_user.setPassword("salasana");
        user2 = ejb.addNewUser(temp_user);
        
        temp_user = new NewUser();
        User user3 = new User();
        temp_user.setName("Testi User3");
        temp_user.setEmail("testi.user3@hotmail.com");
        temp_user.setPassword("salasana");
        user3 = ejb.addNewUser(temp_user);

        System.out.println("user id: " + user1.getId());

        NewBasket temp_basket = new NewBasket();
        Basket basket1 = new Basket();
        temp_basket.setName("Project1");
        temp_basket.setUsers(null);
        temp_basket.setContacts(null);
        temp_basket.setTasks(null);
        temp_basket.setOwner(user1);
        basket1 = ejb.addNewBasket(temp_basket);

        temp_basket = new NewBasket();
        Basket basket2 = new Basket();
        temp_basket.setName("Project2");
        temp_basket.setUsers(null);
        temp_basket.setContacts(null);
        temp_basket.setTasks(null);
        temp_basket.setOwner(user2);
        basket2 = ejb.addNewBasket(temp_basket);

        user1 = ejb.setBasketToUser(user1, basket1);
        user2 = ejb.setBasketToUser(user2, basket2);
        user3 = ejb.setBasketToUser(user3, basket1);

        cal.set(2015, 3, 20);
        Date date = cal.getTime();
        Integer type = 1;
        String description = "Contact1";
        Contact contact1 = ejb.addContact(basket1, person, date, type, description, null, null);

        cal.set(2015, 3, 25);
        date = cal.getTime();
        type = 1;
        description = "Contact2";
        Contact contact2 = ejb.addContact(basket1, person, date, type, description, null, null);

        cal.set(2015, 4, 5);
        date = cal.getTime();
        type = 1;
        description = "Contact3";
        Contact contact3 = ejb.addContact(basket1, person, date, type, description, null, null);

        cal.set(2015, 4, 05);
        date = cal.getTime();
        type = 1;
        description = "Contact4";
        Contact contact4 = ejb.addContact(basket2, person2, date, type, description, null, null);

        task1 = ejb.addTask(basket1, task1.getPerson(), task1.getCompany(), task1.getStartDate(), task1.getDueDate(),
                task1.getDescription(), task1.isDone(), contact1, null);

        task2 = ejb.addTask(basket1, task2.getPerson(), task2.getCompany(), task2.getStartDate(), task2.getDueDate(),
                task2.getDescription(), task2.isDone(), contact2, contact1);

        task3 = ejb.addTask(basket1, task3.getPerson(), task3.getCompany(), task3.getStartDate(),
                task3.getDueDate(), task3.getDescription(), task3.isDone(), contact3, contact2);

        task4 = ejb.addTask(basket2, task4.getPerson(), task4.getCompany(), task4.getStartDate(),
                task4.getDueDate(), task4.getDescription(), task4.isDone(), contact4, null);

        contact1 = ejb.addFollowupPreviousToContact(contact1, task2, task1);
        contact2 = ejb.addFollowupPreviousToContact(contact2, task3, task2);
        contact3 = ejb.addFollowupPreviousToContact(contact3, null, task3);
        contact4 = ejb.addFollowupPreviousToContact(contact4, null, task4);

        List<User> users = new ArrayList<>();
        List<Contact> contacts = new ArrayList();
        List<Task> tasks = new ArrayList();

        users.add(user1);
        users.add(user3);

        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);

        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);

        basket1.setContacts(contacts);
        basket1.setTasks(tasks);
        basket1.setUsers(users);
        basket1.setOwner(user1);

        basket1 = ejb.MergeBasket(basket1);

        users.clear();
        contacts.clear();
        tasks.clear();

        tasks.add(task4);
        contacts.add(contact4);
        users.add(user2);

        basket2.setContacts(contacts);
        basket2.setTasks(tasks);
        basket2.setUsers(users);
        basket2.setOwner(user1);

        basket2 = ejb.MergeBasket(basket2);

        setCompanyList(ejb.listCompanies());
        setPersonList(ejb.listPersons());
        setTaskList(ejb.listTasks());

        setMessage("Database populated with test data!");
    }

    public String getName() {
        return Constants.ADMIN_APP_NAME;
    }

    /**
     * @return the searchText
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * @param searchText the searchText to set
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the companyList
     */
    public List<Company> getCompanyList() {
        return companyList;
    }

    /**
     * @param companyList the companyList to set
     */
    public void setCompanyList(List<Company> companyList) {
        this.companyList = companyList;
    }

    /**
     * @return the personList
     */
    public List<Person> getPersonList() {
        return personList;
    }

    /**
     * @param personList the personList to set
     */
    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

    /**
     * @return the contactList
     */
    public List<Contact> getContactList() {
        return contactList;
    }

    /**
     * @param contactList the contactList to set
     */
    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }

    /**
     * @return the taskList
     */
    public List<Task> getTaskList() {
        return taskList;
    }

    /**
     * @param taskList the taskList to set
     */
    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    /**
     * @return the midleInclude
     */
    public String getMiddleInclude() {
        return middleInclude;
    }

    /**
     * @param middleInclude the midleInclude to set
     */
    public void setMiddleInclude(String middleInclude) {
        this.middleInclude = middleInclude;
    }
}
