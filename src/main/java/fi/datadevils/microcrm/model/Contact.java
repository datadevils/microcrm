/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.microcrm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Samuli
 */
@Entity
@Table(name="contact")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contact implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name="person_id", nullable=false)
    private Person person;
    
    @Temporal(TemporalType.DATE)
    @Column(name="date", nullable=false)
    private Date date;
    
    @Column(name="contact_type", nullable=false)
    private Integer contactType;
    
    @Column(name="description", nullable=false)
    private String description;
    
    @OneToOne(mappedBy="previous",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @XmlTransient
     private Task followUp;
    
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="task_id")
    private Task previous;
    
    @ManyToOne
    @JoinColumn(name="basket_id")
    private Basket basket;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contact)) {
            return false;
        }
        Contact other = (Contact) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fi.datadevils.microcrm.model.Contact[ id=" + id + " ]";
    }

    /**
     * @return the basket
     */
    public Basket getBasket() {
        return basket;
    }

    /**
     * @param basket the basket to set
     */
    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    /**
     * @return the person
     */
    public Person getPerson() {
        return person;
    }

    /**
     * @param person the person to set
     */
    public void setPerson(Person person) {
        this.person = person;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the contactType
     */
    public Integer getContactType() {
        return contactType;
    }

    /**
     * @param contactType the contactType to set
     */
    public void setContactType(Integer contactType) {
        this.contactType = contactType;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the previous
     */
    public Task getPrevious() {
        return previous;
    }

    /**
     * @param previous the previous to set
     */
    public void setPrevious(Task previous) {
        this.previous = previous;
    }

    /**
     * @return the followUp
     */
    public Task getFollowUp() {
        return followUp;
    }

    /**
     * @param followUp the followUp to set
     */
    public void setFollowUp(Task followUp) {
        this.followUp = followUp;
    }
    
}
