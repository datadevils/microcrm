/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.microcrm.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Samuli
 */
@Entity
@Table(name="user")
@XmlAccessorType(XmlAccessType.FIELD)
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="name", nullable=false)
    private String name;
    
    @Column(name="email", unique=true, nullable=false)
    private String email;
    
    @Column(name="admin")
    private boolean adminRole;

    @Lob
    @Column(name="hash")
    private byte[] hash;
    
    @Column(name="salt")
    private String salt;
    
    @OneToOne
    @JoinColumn(name="photo_id")
    private Image photo;
    
   // @ManyToMany(mappedBy="users", fetch=FetchType.EAGER)
   // private List<Basket> baskets;
    
   @ManyToOne(fetch=FetchType.EAGER)
   @XmlTransient
   private Basket basket;
    // private List<Basket> baskets;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fi.datadevils.microcrm.model.User[ id=" + id + " ]";
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the adminRole
     */
    public boolean isAdmin() {
        return adminRole;
    }

    /**
     * @param admin the adminRole to set
     */
    public void setAdmin(boolean admin) {
        this.adminRole = admin;
    }

    /**
     * @return the photo
     */
    public Image getPhoto() {
        return photo;
    }

    /**
     * @param photo the photo to set
     */
    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    /**
     * @return the baskets
     */
    /*
    public List<Basket> getBaskets() {
        return baskets;
    }
    */
    /**
     * @param baskets the baskets to set
     */
    /*
    public void setBaskets(List<Basket> baskets) {
        this.baskets = baskets;
    }
    */
    /**
     * @return the hash
     */
    public byte[] getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    /**
     * @return the salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     * @param salt the salt to set
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * @return the basket
     */
    public Basket getBasket() {
        return basket;
    }

    /**
     * @param basket the basket to set
     */
    public void setBasket(Basket basket) {
        this.basket = basket;
    }
    
}
