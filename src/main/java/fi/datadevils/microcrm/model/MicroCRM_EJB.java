/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.microcrm.model;

import fi.datadevils.microcrm.methods.Authenticate;
import static fi.datadevils.microcrm.methods.Authenticate.generateSalt;
import fi.datadevils.microcrm.rest.classes.NewBasket;
import fi.datadevils.microcrm.rest.classes.NewCompany;
import fi.datadevils.microcrm.rest.classes.NewContact;
import fi.datadevils.microcrm.rest.classes.NewFollowupTask;
import fi.datadevils.microcrm.rest.classes.NewPerson;
import fi.datadevils.microcrm.rest.classes.NewTask;
import fi.datadevils.microcrm.rest.classes.NewUser;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Verneri
 */
@Stateless
public class MicroCRM_EJB {

    @PersistenceContext
    EntityManager em;

    public List<Person> listPersons() {
        return em.createQuery("SELECT e FROM Person e")
                //.setMaxResults(10)
                .getResultList();
    }

    public List<Task> listTasks() {
        return em.createQuery("SELECT e FROM Task e")
                //.setMaxResults(10)
                .getResultList();
    }

    public List<Company> listCompanies() {
        return em.createQuery("SELECT e FROM Company e")
                //.setMaxResults(10)
                .getResultList();
    }

    public List<Contact> listContacts() {
        return em.createQuery("SELECT e FROM Contact e")
                //.setMaxResults(10)
                .getResultList();
    }

    public List<Company> getCompanyByName(String name) {
        return em.createQuery("SELECT e FROM Company e WHERE e.name LIKE :companyName")
                .setParameter("companyName", name)
                .setMaxResults(10)
                .getResultList();
    }

    public List<Person> getPersonsByCompany(Company company) {
        // return em.createQuery("SELECT e FROM Person e WHERE e.company LIKE :companyId")
        return em.createQuery("SELECT e FROM Person e WHERE e.company = :company")
                .setParameter("company", company)
                .setMaxResults(500)
                .getResultList();
    }

    public Contact getFollowUpContact(Task task) {
        Collection contacts = em.createQuery("SELECT u FROM Contact u WHERE u.previous = :task")
                .setParameter("task", task)
                .setMaxResults(1)
                .getResultList();

        if (contacts.size() != 1) {
            return null;
        } else {
            return (Contact) contacts.iterator().next();
        }
    }

    public Task getFollowUpTask(Contact contact) {
        Collection tasks = em.createQuery("SELECT u FROM Task u WHERE u.previous = :contact")
                .setParameter("contact", contact)
                .setMaxResults(1)
                .getResultList();

        if (tasks.size() != 1) {
            return null;
        } else {
            return (Task) tasks.iterator().next();
        }
    }

    public Contact getContactByTask(Task task) {
        Collection contacts = em.createQuery("SELECT u FROM Contact u WHERE u.followUp = :task")
                .setParameter("task", task)
                .setMaxResults(1)
                .getResultList();

        if (contacts.size() != 1) {
            return null;
        } else {
            return (Contact) contacts.iterator().next();
        }
    }

    public Task getTaskByContact(Contact contact) {
        Collection tasks = em.createQuery("SELECT u FROM Task u WHERE u.followUp = :contact")
                .setParameter("contact", contact)
                .setMaxResults(1)
                .getResultList();

        if (tasks.size() != 1) {
            return null;
        } else {
            return (Task) tasks.iterator().next();
        }
    }

    public User getUserByEmail(String email) {
        // return em.createQuery("SELECT e FROM Person e WHERE e.company LIKE :companyId")
        Collection users = em.createQuery("SELECT u FROM User u WHERE u.email = :email")
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList();

        if (users.size() != 1) {
            return null;
        } else {
            return (User) users.iterator().next();
        }
    }
    
    public Basket getBasketByUser(User user) {
        User tempUser = em.find(User.class, user.getId());
        if (tempUser.getBasket() != null) {
            Collection baskets = em.createQuery("SELECT u FROM Basket u WHERE u.id = :basket_id")
                    .setParameter("basket_id", tempUser.getBasket().getId())
                    .setMaxResults(1)
                    .getResultList();

            if (baskets.size() != 1) {
                return null;
            } else {
                return (Basket) baskets.iterator().next();
            }
        } else {
            return null;
        }
    }
 
    public Person getPersonByEmail(String email) {
        // return em.createQuery("SELECT e FROM Person e WHERE e.company LIKE :companyId")
        Collection users = em.createQuery("SELECT u FROM Person u WHERE u.email = :email")
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList();

        if (users.size() != 1) {
            return null;
        } else {
            return (Person) users.iterator().next();
        }
    }

    public void synchroniceToDataBase() {
        em.setFlushMode(FlushModeType.AUTO);
        em.flush();
    }

    public List<User> getUsersByEmail(String email) {
        // return em.createQuery("SELECT e FROM Person e WHERE e.company LIKE :companyId")
        return em.createQuery("SELECT u FROM User u WHERE u.email = :email")
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList();
    }

    public List<Task> getTasksByBasket(Basket basket){
        return em.createQuery("SELECT u FROM Task u WHERE u.basket = :basket")
                .setParameter("basket", basket)
                .getResultList();
    }
    
    public Image addImageToUser(byte[] blob, String filename, String username) {

        User userExists = getUserByEmail(username);
        if (userExists != null) {
            Image temp = new Image();
            temp.setFileName(filename);
            temp.setImage(blob);

            em.persist(temp);

            userExists.setPhoto(temp);

            em.persist(userExists);
            return temp;
        }
        return null;
    }

    public Image addImageToPerson(byte[] blob, String filename, String username) {

        Person personExists = getPersonByEmail(username);
        if (personExists != null) {
            Image temp = new Image();
            temp.setFileName(filename);
            temp.setImage(blob);

            em.persist(temp);

            personExists.setImage(temp);

            em.persist(personExists);
            return temp;
        }
        return null;
    }
    
    public User addNewUser(NewUser user) {

        User userExists = getUserByEmail(user.getEmail());
        if (userExists == null) {

            User temp = new User();
            temp.setName(user.getName());
            temp.setEmail(user.getEmail());
            String salt = generateSalt();
            try {
                temp.setHash(Authenticate.getPasswordHash(user.getPassword(), salt));
                temp.setSalt(salt);
                em.persist(temp);
                return temp;
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(MicroCRM_EJB.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(MicroCRM_EJB.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        return null;
    }

    public Company addCompany(String name) {

        List<Company> nameExists = getCompanyByName(name);
        if (nameExists.isEmpty()) {

            Company temp = new Company();
            temp.setName(name);
            temp.setLogo(null);

            em.persist(temp);
            return temp;
        }
        return null;
    }

    public Person addPerson(String name, String title, String email, String phone, Company company) {
        Person tempPerson = new Person();
        tempPerson.setName(name);
        tempPerson.setEmail(email);
        tempPerson.setPhone(phone);
        tempPerson.setTitle(title);
        tempPerson.setImage(null);

        if (company != null) {
            Company tempComp = em.find(Company.class, company.getId());
            tempPerson.setCompany(tempComp);
        }

        em.persist(tempPerson);

        return tempPerson;
    }

    public Task addNewTask(NewTask newTask) {
        Task tempTask = new Task();
        tempTask.setDescription(newTask.getDescription());
        tempTask.setDone(false);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date start_date = formatter.parse(newTask.getStart_date());
            Date due_date = formatter.parse(newTask.getDue_date());
            tempTask.setStartDate(start_date);
            tempTask.setDueDate(due_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tempTask.setCompany(newTask.getCompany());
        tempTask.setPerson(newTask.getPerson());

        /*
         SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
         String trueFormat = "yyyy-MM-dd";
         try{
         Date start_date = formatter.parse(newTask.getStart_date());
         Date due_date = formatter.parse(newTask.getDue_date());
            
         formatter.applyPattern(trueFormat);
         String trueStart_dateString = formatter.format(start_date);
         String trueDue_dateString = formatter.format(due_date);
         System.out.println(trueStart_dateString);
            
         Date trueStart_date = formatter.parse(trueStart_dateString);
         Date trueDue_date = formatter.parse(trueDue_dateString);
            
         System.out.println(trueStart_date);
         System.out.println(trueDue_date);
            
         tempTask.setStartDate(trueStart_date);
         tempTask.setDueDate(trueDue_date);
         } catch (ParseException e) {
         e.printStackTrace();
         }*/
        em.persist(tempTask);

        return tempTask;
    }

    public Task addNewFollowupTask(NewFollowupTask newFollowupTask) {
        Task tempTask = new Task();
        Contact tempContact = em.find(Contact.class, newFollowupTask.getContact().getId());

        tempTask.setDescription(newFollowupTask.getDescription());
        tempTask.setCompany(newFollowupTask.getCompany());
        tempTask.setPerson(newFollowupTask.getPerson());
        tempTask.setDone(false);
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date start_date = formatter.parse(newFollowupTask.getStart_date());
            Date due_date = formatter.parse(newFollowupTask.getDue_date());
            tempTask.setStartDate(start_date);
            tempTask.setDueDate(due_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        tempTask.setPrevious(tempContact);
        
        em.persist(tempTask);

        tempContact.setFollowUp(tempTask);

        em.merge(tempContact);

        return tempTask;
    }

    public Task addNewContact(NewContact newContact) {

        Contact tempContact = new Contact();
        Task tempTask = em.find(Task.class, newContact.getTask().getId());

        tempContact.setDescription(newContact.getDescription());
        tempContact.setPerson(newContact.getPerson());
        tempContact.setContactType(1);
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = formatter.parse(newContact.getDate());
            tempContact.setDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        tempContact.setPrevious(tempTask);

        em.persist(tempContact);

        tempTask.setDone(newContact.isTaskdone());
        tempTask.setFollowUp(tempContact);

        em.merge(tempTask);

        return tempTask;

    }
   

    public Company addNewCompany(NewCompany newCompany) {

        Company tempCompany = new Company();
        tempCompany.setName(newCompany.getName());

        em.persist(tempCompany);

        return tempCompany;
    }

    public Person addNewPerson(NewPerson newPerson) {

        Person tempPerson = new Person();
        tempPerson.setName(newPerson.getName());
        tempPerson.setTitle(newPerson.getTitle());
        tempPerson.setEmail(newPerson.getEmail());
        tempPerson.setPhone(newPerson.getPhone());
        tempPerson.setCompany(newPerson.getCompany());

        em.persist(tempPerson);

        return tempPerson;
    }

    public Basket addNewBasket(NewBasket newBasket) {
        Basket tempBasket = new Basket();
        tempBasket.setName(newBasket.getName());

        User tempOwner = em.find(User.class, newBasket.getOwner().getId());
        tempBasket.setOwner(tempOwner);

        if (newBasket.getUsers() != null) {
            tempBasket.setUsers(newBasket.getUsers());
        }

        if (newBasket.getContacts() != null) {
            tempBasket.setContacts(newBasket.getContacts());
        }

        if (newBasket.getTasks() != null) {
            tempBasket.setTasks(newBasket.getTasks());
        }

        em.persist(tempBasket);

        return tempBasket;
    }
    
     public User setBasketToUser(User user, Basket basket){
         User tempUser = em.find(User.class, user.getId());
         tempUser.setBasket(basket);
         em.merge(tempUser);
         return tempUser;
     }
     
     public Basket MergeBasket(Basket basket) {
        Basket tempBasket = em.find(Basket.class, basket.getId());
        
        if(basket.getOwner() != null){
            tempBasket.setOwner(basket.getOwner());
        }
        
        if (basket.getUsers() != null) {
            tempBasket.setUsers(basket.getUsers());
        }

        if (basket.getContacts() != null) {
            tempBasket.setContacts(basket.getContacts());
        }

        if (basket.getTasks() != null) {  
            tempBasket.setTasks(basket.getTasks());
        }

        em.merge(tempBasket);

        return tempBasket;
    }
     
    public Task addTask(Basket basket, Person person, Company company, Date start_date, Date due_date,
            String description, boolean done, Contact followUp, Contact previous) {
        Task tempTask = new Task();
        tempTask.setDescription(description);
        tempTask.setDone(done);
        tempTask.setDueDate(due_date);
        tempTask.setStartDate(start_date);

        if (basket != null) {
            Basket tempBasket = em.find(Basket.class, basket.getId());
            tempTask.setBasket(tempBasket);
        }

        if (person != null) {
            Person tempPerson = em.find(Person.class, person.getId());
            tempTask.setPerson(tempPerson);
        }

        if (company != null) {
            Company tempCompany = em.find(Company.class, company.getId());
            tempTask.setCompany(tempCompany);
        }

        if (followUp != null) {
            Contact tempContact = em.find(Contact.class, followUp.getId());
            tempTask.setFollowUp(tempContact);
        }

        if (previous != null) {
            Contact tempContact = em.find(Contact.class, previous.getId());
            tempTask.setPrevious(tempContact);
        }

        em.persist(tempTask);

        return tempTask;

    }

    public Contact addContact(Basket basket, Person person, Date date, Integer contact_type,
            String description, Task followUp, Task previous) {

        Contact tempContact = new Contact();
        tempContact.setDescription(description);
        tempContact.setContactType(contact_type);
        tempContact.setDate(date);
        tempContact.setDescription(description);

        if (person != null) {
            Person tempPerson = em.find(Person.class, person.getId());
            tempContact.setPerson(tempPerson);
        }

        if (followUp != null) {
            Task tempTask = em.find(Task.class, followUp.getId());
            tempContact.setFollowUp(tempTask);
        }

        if (previous != null) {
            Task tempTask = em.find(Task.class, previous.getId());
            tempContact.setPrevious(tempTask);
        }

        if (basket != null) {
            Basket tempBasket = em.find(Basket.class, basket.getId());
            tempContact.setBasket(tempBasket);
        }

        em.persist(tempContact);

        return tempContact;

    }

    public Contact addFollowupPreviousToContact(Contact contact, Task followUp, Task previous) {
        Contact tempContact = em.find(Contact.class, contact.getId());
        if (followUp != null) {
            Task tempTask = em.find(Task.class, followUp.getId());
            tempContact.setFollowUp(tempTask);
        }

        if (previous != null) {
            Task tempTask = em.find(Task.class, previous.getId());
            tempContact.setPrevious(tempTask);
        }
        em.persist(tempContact);
        return contact;
    }

}
