/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.microcrm.rest;

import fi.datadevils.microcrm.methods.ImageHandling;
import fi.datadevils.microcrm.model.Basket;
import fi.datadevils.microcrm.model.Company;
import fi.datadevils.microcrm.model.Contact;
import fi.datadevils.microcrm.model.Image;
import fi.datadevils.microcrm.model.MicroCRM_EJB;
import fi.datadevils.microcrm.model.Person;
import fi.datadevils.microcrm.model.Task;
import fi.datadevils.microcrm.model.User;
import fi.datadevils.microcrm.rest.classes.NewBasket;
import fi.datadevils.microcrm.rest.classes.NewCompany;
import fi.datadevils.microcrm.rest.classes.NewContact;
import fi.datadevils.microcrm.rest.classes.NewFollowupTask;
import fi.datadevils.microcrm.rest.classes.NewPerson;
import fi.datadevils.microcrm.rest.classes.NewTask;
import fi.datadevils.microcrm.rest.classes.NewUser;
import fi.datadevils.microcrm.rest.filters.Secure;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.MimetypesFileTypeMap;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.compress.utils.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 * REST Web Service
 *
 * @author Verneri
 */
@Path("microCRM_handler")
public class MicroCRM_Resource {

    @Context
    private UriInfo context;

    @EJB
    MicroCRM_EJB ejb;

    /**
     * Creates a new instance of microCRM_Resource
     */
    public MicroCRM_Resource() {
    }

    @GET
    @Path("imagebyuser/{email}")
    @Consumes("application/json")
    @Produces({"image/*"})
    public Response getImageByUser(@PathParam("email") String email) {

        System.out.println(email);
        User user = ejb.getUserByEmail(email);

        if (user != null) {
            Image image = user.getPhoto();
            String uploadedFileLocation;
            InputStream myInputStream;
            if (image != null) {
                uploadedFileLocation = "c://uploads/" + image.getFileName();
                myInputStream = new ByteArrayInputStream(image.getImage());
                ImageHandling.writeToFile(myInputStream, uploadedFileLocation);
            } else {
                uploadedFileLocation = "c://uploads/PersonPlaceholder.png";
            }
            File f = new File(uploadedFileLocation);
            String mt = new MimetypesFileTypeMap().getContentType(f);
            return Response.ok(f, mt).build();
        }
        return null;
    }

    @GET
    @Path("imagebyperson/{email}")
    @Consumes("application/json")
    @Produces({"image/*"})
    public Response getImageByPerson(@PathParam("email") String email) {

        System.out.println(email);
        Person person = ejb.getPersonByEmail(email);
        if (person != null) {
            Image image = person.getImage();
            String uploadedFileLocation;
            InputStream myInputStream;
            if (image != null) {
                uploadedFileLocation = "c://uploads/" + image.getFileName();
                myInputStream = new ByteArrayInputStream(image.getImage());
                ImageHandling.writeToFile(myInputStream, uploadedFileLocation);
            } else {
                uploadedFileLocation = "c://uploads//PersonPlaceholder.png";
            }
            File f = new File(uploadedFileLocation);
            String mt = new MimetypesFileTypeMap().getContentType(f);
            return Response.ok(f, mt).build();
        }
        return null;
    }    
    
    /**
     *
     * @return all companies in the database
     */
    @Secure
    @Path("listcompanies")
    @GET
    @Produces("application/json")
    // @Consumes("application/json")
    public List<Company> getCompaniesJson() {
        return ejb.listCompanies();
    }

    /**
     *
     * @param company
     * @return persons listed by company name
     */
    @Secure
    @Path("personsbycompany")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public List<Person> getPersonsByCompanyJson(Company company) {
        return ejb.getPersonsByCompany(company);
    }

    /**
     *
     * @return list of all persons in database
     */
    @Secure
    @Path("listpersons")
    @GET
    @Produces("application/json")
    // @Consumes("application/json")
    public List<Person> getPersonsJson() {
        return ejb.listPersons();
    }

    /**
     *
     * @return list of all tasks in the database
     */
    @Secure
    @Path("listtasks")
    @GET
    @Produces("application/json")
    //@Consumes("application/json")
    public List<Task> getTasksJson() {
        return ejb.listTasks();
    }

    /**
     *
     * @param task
     * @return followup contact by a task
     */
    @Secure
    @Path("followupbytask")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Contact getFollowUpByTaskJson(Task task) {
        Contact contact = ejb.getFollowUpContact(task);
        return contact;
    }

    /**
     *
     * @param contact
     * @return followup task by a contact
     */
    @Secure
    @Path("followupbycontact")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Task getFollowUpByContactJson(Contact contact) {
        Task task = ejb.getFollowUpTask(contact);
        return task;
    }

    /**
     *
     * @param contact
     * @return task by a contact
     */
    @Secure
    @Path("taskbycontact")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Task getTaskbyContactJson(Contact contact) {
        return ejb.getTaskByContact(contact);
    }

    /**
     *
     * @param task
     * @return contact by a task
     */
    @Secure
    @Path("contactbytask")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Contact getContactbyTaskJson(Task task) {
        return ejb.getContactByTask(task);
    }

    /*
     @Secure
     @Path("contactsbytask")
     @POST
     @Produces("application/json")
     @Consumes("application/json")
     public List<Contact> getContactsByTaskJson(Task task) {
     return ejb.getContactByTask(task);
     }
     */
    /**
     * @param email
     * @return user by email
     */
    @Secure
    @Path("user")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public List<User> getUsersByEmailJson(User user) {
        System.out.println("User: " + user.getEmail());
        List<User> users = ejb.getUsersByEmail(user.getEmail());
        if (users.isEmpty() == false) {
            System.out.println(users.iterator().next().getName());
        }
        return users;
    }

    @Path("addNewUser")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public User addNewUserJson(NewUser newUser) {
        System.out.println("New User received!");
        System.out.println("Email : " + newUser.getEmail()
                + "| Name: " + newUser.getName()
                + "| Password: " + newUser.getPassword());

        return ejb.addNewUser(newUser);
    }

    @Secure
    @Path("addNewTask")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Task addNewTaskJson(NewTask newTask) {
        System.out.println("New Task received!");
        System.out.println("Description: " + newTask.getDescription()
                + "| Start Date: " + newTask.getStart_date()
                + "| Due Date: " + newTask.getDue_date()
                + "| Company: " + newTask.getCompany()
                + "| Person: " + newTask.getPerson());

        return ejb.addNewTask(newTask);
    }

    @Secure
    @Path("addNewFollowupTask")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Task addNewFollowupTaskJson(NewFollowupTask newFollowupTask) {
        System.out.println("New Follow up Task received!");
        System.out.println("Description: " + newFollowupTask.getDescription()
                + "| Start Date: " + newFollowupTask.getStart_date()
                + "| Due Date: " + newFollowupTask.getDue_date()
                + "| Company: " + newFollowupTask.getCompany()
                + "| Person: " + newFollowupTask.getPerson()
                + "| Followup contact: " + newFollowupTask.getContact());

        return ejb.addNewFollowupTask(newFollowupTask);
    }

    @Secure
    @Path("addNewCompany")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Company addNewCompanyJson(NewCompany newCompany) {
        System.out.println("New Company received!");
        System.out.println("Name: " + newCompany.getName());

        return ejb.addNewCompany(newCompany);
    }

    @Secure
    @Path("addNewPerson")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Person addNewPersonJson(NewPerson newPerson) {
        System.out.println("New Person received!");
        System.out.println("Name: " + newPerson.getName());
        System.out.println("Title: " + newPerson.getTitle());
        System.out.println("Email: " + newPerson.getEmail());
        System.out.println("Phone: " + newPerson.getPhone());
        System.out.println("Company: " + newPerson.getCompany());

        return ejb.addNewPerson(newPerson);
    }

    @Secure
    @Path("addNewContact")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Task addNewContactJson(NewContact newContact) {
        return ejb.addNewContact(newContact);
    }
    
    /**
     *
     * @param newBasket
     * @return new Basket object
     */
    @Secure
    @Path("addNewBasket")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Basket addNewBasketJson(NewBasket newBasket) {
        return ejb.addNewBasket(newBasket);
    }
    
    /**
     *
     * @param basket
     * @return all baskets that have the parameter set as their basket.
     */
    @Secure
    @Path("tasksbybasket")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public List<Task> getTasksByBasketJson(Basket basket) {
        System.out.println("Basket id" + basket.getId());
        System.out.println("Basket name" + basket.getName());
        System.out.println("Basket owner" + basket.getOwner());
        return ejb.getTasksByBasket(basket);
    }
    
    /**
     *
     * @param user
     * @return basket by the logged in user which is as parameter.
     */
    @Secure
    @Path("basketbyuser")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Basket getBasketByUserJson(User user) {
        System.out.println("User id: " + user.getId());
        System.out.println("User name" + user.getName());
        System.out.println("User email" + user.getEmail());
        return ejb.getBasketByUser(user);
    }
    

    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public void uploadFile(
            @FormDataParam("description") String username,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {

        try {
            byte[] image = IOUtils.toByteArray(uploadedInputStream);
            ejb.addImageToUser(image, fileDetail.getFileName(), username);
            ejb.addImageToPerson(image, fileDetail.getFileName(), username);
        } catch (IOException ex) {
            Logger.getLogger(MicroCRM_Resource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Puts test data to database
     */
    @Path("initdatabase")
    @PUT
    @Produces("application/json")
    @Consumes("application/json")
    public void initDatabaseJson() {
        
        /*
         Company company = new Company();
         company.setName("Patria");
         company = ejb.addCompany(company.getName());

         if (company != null) {

         Person person = new Person();
         person.setCompany(company);
         person.setEmail("pekka.tapiola@patria.fi");
         person.setName("Pekka Tapiola");
         person.setTitle("Osastopäällikkö");
         person.setPhone("0400 123321");
         person = ejb.addPerson(person.getName(), person.getTitle(), person.getEmail(),
         person.getPhone(), company);

         person = new Person();
         person.setCompany(company);
         person.setEmail("matti.tapio@patria.fi");
         person.setName("Matti Tapio");
         person.setTitle("Siivooja");
         person.setPhone("041 1234556");
         person = ejb.addPerson(person.getName(), person.getTitle(), person.getEmail(),
         person.getPhone(), company);

         person = new Person();
         person.setCompany(company);
         person.setEmail("teppo.tapio@patria.fi");
         person.setName("Teppo Tapio");
         person.setTitle("Johtaja");
         person.setPhone("041 12345");
         person = ejb.addPerson(person.getName(), person.getTitle(), person.getEmail(),
         person.getPhone(), company);

         Company company2 = new Company();
         company2.setName("Accenture");
         company2 = ejb.addCompany(company2.getName());
         Person person2 = new Person();
         person2.setCompany(company2);
         person2.setEmail("heikki.peränne@accenture.fi");
         person2.setName("Heikki Peränne");
         person2.setTitle("Markkinointipäällikkö");
         person2.setPhone("0400 123456");
         person2 = ejb.addPerson(person2.getName(), person2.getTitle(), person2.getEmail(),
         person2.getPhone(), company2);

         Company company3 = new Company();
         company3.setName("Blancco");
         company3 = ejb.addCompany(company3.getName());
         Person person3 = new Person();
         person3.setCompany(company3);
         person3.setEmail("anja.mäkirinne@blancco.fi");
         person3.setName("Anja Mäkirinne");
         person3.setTitle("Henkilöstöpäällikkö");
         person3.setPhone("0400 654321");
         person3 = ejb.addPerson(person3.getName(), person3.getTitle(), person3.getEmail(),
         person3.getPhone(), company3);

         Task task1 = new Task();
         Calendar cal = Calendar.getInstance();
         cal.set(2015, 3, 15);
         Date dueDate = cal.getTime();
         cal.set(2015, 3, 10);
         Date startDate = cal.getTime();
         task1.setDueDate(dueDate);
         task1.setStartDate(startDate);
         task1.setDescription("Lähetä hakemus");
         task1.setPerson(person);
         task1.setDone(false);
         task1 = ejb.addTask(null, person, company, startDate, dueDate,
         task1.getDescription(), task1.isDone(), null, null);

         Task task2 = new Task();
         cal.set(2015, 3, 25);
         dueDate = cal.getTime();
         cal.set(2015, 3, 20);
         startDate = cal.getTime();
         task2.setDueDate(dueDate);
         task2.setStartDate(startDate);
         task2.setDescription("Kysy hakemuksen tilannetta");
         task2.setPerson(person);
         task2.setDone(false);
         task2 = ejb.addTask(null, person, company, startDate, dueDate,
         task2.getDescription(), task2.isDone(), null, null);

         Task task3 = new Task();
         cal.set(2015, 3, 16);
         dueDate = cal.getTime();
         cal.set(2015, 3, 15);
         startDate = cal.getTime();
         task3.setDueDate(dueDate);
         task3.setStartDate(startDate);
         task3.setDescription("Kysy lisätietoja avoimesta työpaikasta");
         task3.setPerson(person);
         task3.setDone(false);
         task3 = ejb.addTask(null, person2, company2, startDate,
         dueDate, task3.getDescription(), task3.isDone(), null, null);

         /*            
         cal.set(2015, 3, 20);
         Date date = cal.getTime();
         Integer type = 1;
         String description = "Vastaa työhönotosta";
         Contact contact1 = ejb.addContact(person, date, type, description, task2, task1, null);

         cal.set(2015, 3, 25);
         date = cal.getTime();
         type = 1;
         description = "Vastaa työhönotosta";
         Contact contact2 = ejb.addContact(person, date, type, description, null, task2, null);

         cal.set(2015, 4, 5);
         date = cal.getTime();
         type = 1;
         description = "Antaa lisätietoja työpaikasta";
         Contact contact3 = ejb.addContact(person, date, type, description, null, task3, null);

		


         NewUser u = new NewUser();
         u.setEmail("sahonen@kolumbus.fi");
         u.setName("Samuli Ahonen");
         u.setPassword("testi");

            
         User user = ejb.addNewUser(u);

         }  */
    }
}
