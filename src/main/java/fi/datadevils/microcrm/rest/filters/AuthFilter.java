/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.microcrm.rest.filters;

import fi.datadevils.microcrm.methods.Authenticate;
import fi.datadevils.microcrm.model.MicroCRM_EJB;
import fi.datadevils.microcrm.model.User;
import javax.ejb.EJB;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Samuli
 */
@Secure
@Provider
public class AuthFilter implements ContainerRequestFilter {

    @EJB
    MicroCRM_EJB ejb;
    
    @Override
    public void filter(ContainerRequestContext containerRequest) {
        
        System.out.println("Auth Filter activated!!!");
         
        //Get the authentification from header
        String auth = containerRequest.getHeaderString("authorization");
 
        System.out.println(auth);
        
        if(auth == null){
            System.out.println("Login failed");
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
 
        String[] login = Authenticate.decodeAuthorization(auth);
 
        // login[0] = email
        // login[0] = password
        
     
        if(login == null || login.length != 2){
            System.out.println("Login failed");
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
        
        System.out.println(login[0] + " " + login[1]);
        
        User user = ejb.getUserByEmail(login[0]);     
        
        if(user != null){
            System.out.println(user.getName());
        } else {
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
        
        user = Authenticate.authenticateUser(user, login[1]);
 
        //Our system refuse login and password
        if(user == null){
            System.out.println("Login failed");
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
        System.out.println("Login success");

    }
}
