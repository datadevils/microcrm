/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.microcrm.rest.classes;

import fi.datadevils.microcrm.model.Person;
import fi.datadevils.microcrm.model.Task;

/**
 *
 * @author Verneri
 */
public class NewContact {
        
        private Person person;
        private String date;
        private String Description;
        private boolean taskdone;
        private Task task;
        private Integer type;

    /**
     * @return the person
     */
    public Person getPerson() {
        return person;
    }

    /**
     * @param person the person to set
     */
    public void setPerson(Person person) {
        this.person = person;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description the Description to set
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return the taskdone
     */
    public boolean isTaskdone() {
        return taskdone;
    }

    /**
     * @param taskdone the taskdone to set
     */
    public void setTaskdone(boolean taskdone) {
        this.taskdone = taskdone;
    }

    /**
     * @return the task
     */
    public Task getTask() {
        return task;
    }

    /**
     * @param task the task to set
     */
    public void setTask(Task task) {
        this.task = task;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }
}
